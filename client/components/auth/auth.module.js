'use strict';

angular.module('labsApp.auth', [
  'labsApp.constants',
  'labsApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
