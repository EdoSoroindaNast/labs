'use strict';

angular.module('labsApp.admin', [
  'labsApp.auth',
  'ui.router'
]);
