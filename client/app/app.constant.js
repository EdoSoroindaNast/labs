(function(angular, undefined) {
'use strict';

angular.module('labsApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);